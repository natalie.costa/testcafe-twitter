import {Selector} from 'testcafe';

fixture `Twitter test`
    .page `https://twitter.com/`
    .beforeEach(async t => {
        await t.maximizeWindow();

    });

test('Logando no Twitter e tweetando', async t => {
    let twitter_user = Selector('form[data-component=login_callout] div').child('input[type=text]');
    let twitter_password = Selector('form[data-component=login_callout] div').child('input[placeholder=Password]');
    let twitter_login_button = Selector('form[data-component=login_callout]').child('input[type=submit]');
    let twitter_tweet = Selector('div[data-contents=true] div').child('div');
    let button_for_tweet = Selector('div[data-testid=tweetButtonInline]').child('div');
    let assert_tweet = Selector('div[id=react-root] div div div div div div div').child('span');

    await t
        .typeText(twitter_user , 'natalieyel@outlook.com')
        .typeText(twitter_password, '************')
        .click(twitter_login_button)
        .typeText(twitter_tweet, 'terminando, amém')
        .click(button_for_tweet)
        .expect(assert_tweet.innerText).eql('Your Tweet was sent.', 'Toast diferente do esperado');

});